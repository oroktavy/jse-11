package ru.aushakov.tm.api.service;

import ru.aushakov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    void add(Task task);

    Task add(String name, String description);

    void remove(Task task);

    void clear();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneById(String id);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

    Task updateOneById(String id, String name, String description);

    Task updateOneByIndex(Integer index, String name, String description);

}
