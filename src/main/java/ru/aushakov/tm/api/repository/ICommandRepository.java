package ru.aushakov.tm.api.repository;

import ru.aushakov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
