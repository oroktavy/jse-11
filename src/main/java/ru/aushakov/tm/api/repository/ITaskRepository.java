package ru.aushakov.tm.api.repository;

import ru.aushakov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    void add(Task task);

    void remove(Task task);

    void clear();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneById(String id);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

    Task updateOneById(String id, String name, String description);

    Task updateOneByIndex(Integer index, String name, String description);

}
