package ru.aushakov.tm.api.controller;

public interface ITaskController {

    void showList();

    void create();

    void clear();

    void showOneById();

    void showOneByIndex();

    void showOneByName();

    void removeOneById();

    void removeOneByIndex();

    void removeOneByName();

    void updateOneById();

    void updateOneByIndex();

}
